﻿
namespace GeometricApp {
    partial class Form1 {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lblSquare = new System.Windows.Forms.Label();
            this.lblSideOfSquare = new System.Windows.Forms.Label();
            this.tbSideOfSquare = new System.Windows.Forms.TextBox();
            this.btnAddSquare = new System.Windows.Forms.Button();
            this.lblCircle = new System.Windows.Forms.Label();
            this.lblRadius = new System.Windows.Forms.Label();
            this.tbRadius = new System.Windows.Forms.TextBox();
            this.btnAddCircle = new System.Windows.Forms.Button();
            this.lblRectangle = new System.Windows.Forms.Label();
            this.lbl1SideOfRectangle = new System.Windows.Forms.Label();
            this.tb1SideOfRectangle = new System.Windows.Forms.TextBox();
            this.tb2SideOfRectangle = new System.Windows.Forms.TextBox();
            this.lbl2SideOfRectangle = new System.Windows.Forms.Label();
            this.btnAddRectangle = new System.Windows.Forms.Button();
            this.tbShapes = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblSquare
            // 
            this.lblSquare.AutoSize = true;
            this.lblSquare.Location = new System.Drawing.Point(35, 9);
            this.lblSquare.Name = "lblSquare";
            this.lblSquare.Size = new System.Drawing.Size(47, 15);
            this.lblSquare.TabIndex = 0;
            this.lblSquare.Text = "Kvadrat";
            // 
            // lblSideOfSquare
            // 
            this.lblSideOfSquare.AutoSize = true;
            this.lblSideOfSquare.Location = new System.Drawing.Point(35, 44);
            this.lblSideOfSquare.Name = "lblSideOfSquare";
            this.lblSideOfSquare.Size = new System.Drawing.Size(55, 15);
            this.lblSideOfSquare.TabIndex = 1;
            this.lblSideOfSquare.Text = "Stranica :";
            // 
            // tbSideOfSquare
            // 
            this.tbSideOfSquare.Location = new System.Drawing.Point(35, 79);
            this.tbSideOfSquare.Name = "tbSideOfSquare";
            this.tbSideOfSquare.Size = new System.Drawing.Size(108, 23);
            this.tbSideOfSquare.TabIndex = 2;
            // 
            // btnAddSquare
            // 
            this.btnAddSquare.Location = new System.Drawing.Point(35, 125);
            this.btnAddSquare.Name = "btnAddSquare";
            this.btnAddSquare.Size = new System.Drawing.Size(108, 26);
            this.btnAddSquare.TabIndex = 3;
            this.btnAddSquare.Text = "Dodaj kvadrat";
            this.btnAddSquare.UseVisualStyleBackColor = true;
            this.btnAddSquare.Click += new System.EventHandler(this.btnAddSquare_Click);
            // 
            // lblCircle
            // 
            this.lblCircle.AutoSize = true;
            this.lblCircle.Location = new System.Drawing.Point(40, 187);
            this.lblCircle.Name = "lblCircle";
            this.lblCircle.Size = new System.Drawing.Size(52, 15);
            this.lblCircle.TabIndex = 4;
            this.lblCircle.Text = "Kružnica";
            // 
            // lblRadius
            // 
            this.lblRadius.AutoSize = true;
            this.lblRadius.Location = new System.Drawing.Point(40, 225);
            this.lblRadius.Name = "lblRadius";
            this.lblRadius.Size = new System.Drawing.Size(51, 15);
            this.lblRadius.TabIndex = 5;
            this.lblRadius.Text = "Radijus :";
            // 
            // tbRadius
            // 
            this.tbRadius.Location = new System.Drawing.Point(40, 263);
            this.tbRadius.Name = "tbRadius";
            this.tbRadius.Size = new System.Drawing.Size(108, 23);
            this.tbRadius.TabIndex = 6;
            // 
            // btnAddCircle
            // 
            this.btnAddCircle.Location = new System.Drawing.Point(40, 316);
            this.btnAddCircle.Name = "btnAddCircle";
            this.btnAddCircle.Size = new System.Drawing.Size(108, 26);
            this.btnAddCircle.TabIndex = 7;
            this.btnAddCircle.Text = "Dodaj kružnicu";
            this.btnAddCircle.UseVisualStyleBackColor = true;
            // 
            // lblRectangle
            // 
            this.lblRectangle.AutoSize = true;
            this.lblRectangle.Location = new System.Drawing.Point(40, 382);
            this.lblRectangle.Name = "lblRectangle";
            this.lblRectangle.Size = new System.Drawing.Size(70, 15);
            this.lblRectangle.TabIndex = 8;
            this.lblRectangle.Text = "Pravokutnik";
            // 
            // lbl1SideOfRectangle
            // 
            this.lbl1SideOfRectangle.AutoSize = true;
            this.lbl1SideOfRectangle.Location = new System.Drawing.Point(40, 423);
            this.lbl1SideOfRectangle.Name = "lbl1SideOfRectangle";
            this.lbl1SideOfRectangle.Size = new System.Drawing.Size(61, 15);
            this.lbl1SideOfRectangle.TabIndex = 9;
            this.lbl1SideOfRectangle.Text = "Stranica 1:";
            // 
            // tb1SideOfRectangle
            // 
            this.tb1SideOfRectangle.Location = new System.Drawing.Point(40, 460);
            this.tb1SideOfRectangle.Name = "tb1SideOfRectangle";
            this.tb1SideOfRectangle.Size = new System.Drawing.Size(108, 23);
            this.tb1SideOfRectangle.TabIndex = 10;
            // 
            // tb2SideOfRectangle
            // 
            this.tb2SideOfRectangle.Location = new System.Drawing.Point(40, 541);
            this.tb2SideOfRectangle.Name = "tb2SideOfRectangle";
            this.tb2SideOfRectangle.Size = new System.Drawing.Size(108, 23);
            this.tb2SideOfRectangle.TabIndex = 11;
            // 
            // lbl2SideOfRectangle
            // 
            this.lbl2SideOfRectangle.AutoSize = true;
            this.lbl2SideOfRectangle.Location = new System.Drawing.Point(40, 505);
            this.lbl2SideOfRectangle.Name = "lbl2SideOfRectangle";
            this.lbl2SideOfRectangle.Size = new System.Drawing.Size(61, 15);
            this.lbl2SideOfRectangle.TabIndex = 12;
            this.lbl2SideOfRectangle.Text = "Stranica 2:";
            // 
            // btnAddRectangle
            // 
            this.btnAddRectangle.Location = new System.Drawing.Point(40, 589);
            this.btnAddRectangle.Name = "btnAddRectangle";
            this.btnAddRectangle.Size = new System.Drawing.Size(122, 26);
            this.btnAddRectangle.TabIndex = 13;
            this.btnAddRectangle.Text = "Dodaj pravokutnik";
            this.btnAddRectangle.UseVisualStyleBackColor = true;
            // 
            // tbShapes
            // 
            this.tbShapes.Location = new System.Drawing.Point(210, 44);
            this.tbShapes.Multiline = true;
            this.tbShapes.Name = "tbShapes";
            this.tbShapes.Size = new System.Drawing.Size(296, 571);
            this.tbShapes.TabIndex = 14;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(544, 662);
            this.Controls.Add(this.tbShapes);
            this.Controls.Add(this.btnAddRectangle);
            this.Controls.Add(this.lbl2SideOfRectangle);
            this.Controls.Add(this.tb2SideOfRectangle);
            this.Controls.Add(this.tb1SideOfRectangle);
            this.Controls.Add(this.lbl1SideOfRectangle);
            this.Controls.Add(this.lblRectangle);
            this.Controls.Add(this.btnAddCircle);
            this.Controls.Add(this.tbRadius);
            this.Controls.Add(this.lblRadius);
            this.Controls.Add(this.lblCircle);
            this.Controls.Add(this.btnAddSquare);
            this.Controls.Add(this.tbSideOfSquare);
            this.Controls.Add(this.lblSideOfSquare);
            this.Controls.Add(this.lblSquare);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSquare;
        private System.Windows.Forms.Label lblSideOfSquare;
        private System.Windows.Forms.TextBox tbSideOfSquare;
        private System.Windows.Forms.Button btnAddSquare;
        private System.Windows.Forms.Label lblCircle;
        private System.Windows.Forms.Label lblRadius;
        private System.Windows.Forms.TextBox tbRadius;
        private System.Windows.Forms.Button btnAddCircle;
        private System.Windows.Forms.Label lblRectangle;
        private System.Windows.Forms.Label lbl1SideOfRectangle;
        private System.Windows.Forms.TextBox tb1SideOfRectangle;
        private System.Windows.Forms.TextBox tb2SideOfRectangle;
        private System.Windows.Forms.Label lbl2SideOfRectangle;
        private System.Windows.Forms.Button btnAddRectangle;
        private System.Windows.Forms.TextBox tbShapes;
    }
}

