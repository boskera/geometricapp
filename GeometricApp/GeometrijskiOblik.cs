﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeometricApp {
    abstract class GeometrijskiOblik {
        public abstract string Opis();
    }
}
